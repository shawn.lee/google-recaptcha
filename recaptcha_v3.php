<?php

require_once __DIR__."/Google/Recaptcha/recaptcha.class.php";

use Google\Recaptcha\Recaptcha;

# google recaptcha key & secret : https://www.google.com/recaptcha/admin
$siteKey = '6LfFLKkUAAAAAINuidPfqj_KqpSvAC3zUAAtYcPL';
$secret = '6LfFLKkUAAAAACpGX6DjL6jZIHesoTRhoIery6z1';
# google Language codes : https://developers.google.com/recaptcha/docs/language
$locale = 'zh-TW';

if($_POST){
    $gRecaptchaResponse = $_POST['g-recaptcha-response'];
    $remoteIp = $_SERVER['REMOTE_ADDR'];

    $recaptcha = new Recaptcha($siteKey,$secret);
    $verify = $recaptcha->verify($gRecaptchaResponse,$remoteIp);

}

?>
<html>
    <head>
        <title>reCAPTCHA v3 demo</title>
        <script src="https://www.google.com/recaptcha/api.js?render=<?=$siteKey?>&hl=<?=$locale?>"></script>
        <script>
            grecaptcha.ready(function() {
                grecaptcha.execute('<?=$siteKey?>', {action: 'homepage'}).then(function(token) {
                    console.log(token);
                    document.getElementById("test").value = token;
                }); 
            });
        </script>
    </head>
    <body>
        <form action="" method="POST">
            <div id='captcha'></div>
            google recaptcha ： <input id="test" name="g-recaptcha-response" type="text" >
            <br>
            <input type="submit" value="submit">
        </form>
        <?php if($_POST){?>
            <?php if($verify->isSuccess()){?>
                <div>"驗證成功"</div>
                <ul>
                    <li>分數：<?=$verify->getScore()?></li>
                    <li>動作：<?=$verify->getAction()?></li>
                </ul>
            <?php }else{?>
                <div>"驗證失敗"</div>
                <ul>
                    <?php foreach ($verify->getErrorCode() as $key => $value) { ?>
                        <li><?=$value?></li>
                    <?php } ?>
                </ul>
            <?php }?>
        <?php }?>
    </body>
</html>