<?php
require_once __DIR__."/Google/Recaptcha/recaptcha.class.php";

use Google\Recaptcha\Recaptcha;

# google recaptcha key & secret : https://www.google.com/recaptcha/admin
$siteKey = '6Lem6x4UAAAAAMNpklurBIHGkt3N_sDE3gTQ6oeA';
$secret = '6Lem6x4UAAAAAKoAq5vuuDMsZr3ALD-TrykHhbp3';
# google Language codes : https://developers.google.com/recaptcha/docs/language
$locale = 'zh-TW';

if($_POST){
    $gRecaptchaResponse = $_POST['g-recaptcha-response'];
    $remoteIp = $_SERVER['REMOTE_ADDR'];

    $recaptcha = new Recaptcha($siteKey,$secret);
    $verify = $recaptcha->verify($gRecaptchaResponse,$remoteIp);
}

?>

<html>
  <head>
    <title>reCAPTCHA demo</title>
    <script src="https://www.google.com/recaptcha/api.js?render=explicit&hl=<?=$locale?>" async defer></script>
    <script type="text/javascript">
      function pageInit(){
        grecaptcha.render('captcha', {
          'sitekey' : '<?=$siteKey?>', //申請API網站KEY
        });
      }
    </script>
  </head>
  <body onload="pageInit()">
    <form action="" method="POST">
    <div id='captcha'></div>
      <input type="submit" value="submit">
    </form>
    <?php if($_POST){?>
            <?php if($verify->isSuccess()){?>
                <div>"驗證成功"</div>
            <?php }else{?>
                <div>"驗證失敗"</div>
                <ul>
                    <?php foreach ($verify->getErrorCode() as $key => $value) { ?>
                        <li><?=$value?></li>
                    <?php } ?>
                </ul>
            <?php }?>
        <?php }?>
  </body>
</html>