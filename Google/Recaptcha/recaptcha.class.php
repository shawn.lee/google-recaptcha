<?php
namespace Google\Recaptcha;

# google 驗證機器人 https://developers.google.com/recaptcha
class Recaptcha 
{	
	# 查詢google recaptcha key : https://www.google.com/recaptcha/admin
	private $siteKey;
	private $secret;
	# 驗證API URL
	private $google_recaptcha_api = 'https://www.google.com/recaptcha/api/siteverify';

	private $verify;

	public function __construct($siteKey,$secret){
		$this->siteKey = $siteKey;
		$this->secret = $secret;
	}

	# 通知驗證API 結果
	public function verify($gRecaptchaResponse, $remoteIp){
		$requestBody = array(
			'response' => $gRecaptchaResponse,
			'secret' => $this->secret
		);

		if(!is_null($remoteIp))
			$requestBody['remoteip'] = $remoteIp;

		$this->verify = $this->post($this->google_recaptcha_api, $requestBody);
		
		return $this;
	}
	# verify response
	public function response(){
		return $this->verify;
	}
	# verify成功
	public function isSuccess(){
		return is_bool($this->verify->success) ? $this->verify->success : null;
	}
	# verify分數
	public function getScore(){
		return isset($this->verify->score) ? $this->verify->score : null;
	}
	# verify方式
	public function getAction(){
		return isset($this->verify->action) ? $this->verify->action : null;
	}
	# verify失敗原因
	public function getErrorCode(){
		return is_array($this->verify->{'error-codes'}) ? $this->verify->{'error-codes'} : null;
	}
	#call google server api
	private function post($url, $requestBody){
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($requestBody));

		$result = curl_exec($ch);
		$errorNo = curl_errno($ch);;
		curl_close($ch);

		return ($errorNo === 0 ) ? json_decode($result) : $result;
	}

}

?>